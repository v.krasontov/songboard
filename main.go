package main

import (
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func getStreamer(path string) (s beep.StreamSeekCloser, f beep.Format, err error) {
	fp, err := os.Open(path)
	if err != nil {
		return
	}
	return mp3.Decode(fp)
}

/******************************************************************************
* patterns
*
* we have a queue with limit where we can push beep.Streamers upon request from
* a web server
* it's a fifo queue
* queue ops must be concurrency safe
* the speaker must wait and fetch things from the queue, popping and playing
* the currently playing thingy must be publicly accessible for request like
* pause
* it is already concurrency safe because we have to lock / unlock anyways
* and mute (volume change?)
* through type assertions we see if we can pause / mute
*
******************************************************************************/

/******************************************************************************
* general notes
*
* we should use buffer (for short files)
* figure out where to get the files
* scope: everything (like handlers) can be methods of a Player type, which is a
* struct containing the queue, the player, the buffers, ...
*
******************************************************************************/

/******************************************************************************
* web server
*
* routes:
* - pause
* - mute
* - list available songs
* - route for playing song (for each available song)
*
* can we make the queue and the currently playing song available through a web
* server as well?
*
******************************************************************************/

func main() {
	testFilePath := "/home/val/Downloads/cute-short.mp3"
	streamer, format, err := getStreamer(testFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer streamer.Close()

	otherStreamer, _, err := getStreamer(testFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer otherStreamer.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	done := make(chan bool)

	speaker.Play(otherStreamer)
	time.Sleep(2 * time.Second)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		time.Sleep(2 * time.Second)
		done <- true
	})))

	<-done
}
